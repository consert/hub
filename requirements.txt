jupyterhub==4.1.5
jupyterhub-idle-culler==1.3.1
jupyter_server==2.14.0
jupyterlab==4.1.8
ipywidgets==8.1.2
