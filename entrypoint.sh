#!/bin/env bash
# shellcheck disable=SC1091,SC1090
if [ "$(id -u)" = "0" ]; then
  # this is meant to only run once (init), to make sure any volumes are owned by jamie
  mkdir -p /home/jamie/data && chown -R jamie:jamie /home/jamie/data
  ls -lah /home/jamie
else
  INITIAL_PASSWORD=${INITIAL_PASSWORD:-Ch@ng3M3}
  # https://github.com/jupyterhub/jupyterhub/issues/486#issuecomment-287621407
  sudo groupadd shadow > /dev/null 2>&1 || true
  sudo chgrp shadow /etc/shadow > /dev/null 2>&1 || true
  sudo chmod g+r /etc/shadow > /dev/null 2>&1 || true
  sudo usermod -a -G shadow "$(whoami)" > /dev/null 2>&1 || true
  #
  echo "$(whoami):${INITIAL_PASSWORD}" | sudo chpasswd
  mkdir -p "${HOME}/.local/share/jupyter"
  J_CONF="${HOME}/.local/share/jupyter/jupyterhub_config.py"
  cd "${HOME}/.local/share/jupyter" || exit 1
  "${HOME}/.local/bin/jupyterhub" --generate-config -f "${J_CONF}"

  JUPYTER_PORT=${JUPYTER_PORT:-8000}
  export JUPYTER_PORT="${JUPYTER_PORT}"

  sed "s/\(# c\.JupyterHub\.port\)\(.*\=\)\(.*\)/c.JupyterHub.port = ${JUPYTER_PORT}/" "${J_CONF}" > j && mv j "${J_CONF}"
  sed "s/\(# c\.\)\(.*\=.*\)\(127\.0.\0\.1\)/c.\20.0.0.0/g" "${J_CONF}" > j && mv j "${J_CONF}"
  echo 'c.NotebookApp.tornado_settings = {"websocket_max_message_size": 100 * 1024 * 1024}' >> "${J_CONF}"

  _default_kernel="${HOME}/.local/share/jupyter/kernels/python3/kernel.json"
  if [ -f "${_default_kernel}" ]; then
      py_version="$(python3 --version | awk '{print $2}')"
      sed "s/Python 3 (ipykernel)\"/Python ${py_version} (ipykernel)\"/" "${_default_kernel}"  > j && mv j "${_default_kernel}"
  fi
  jupyterhub -f "${HOME}/.local/share/jupyter/jupyterhub_config.py"
fi
