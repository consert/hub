# Hub

Jupyterhub with python3.{8,9,10,11,12} kernels.

After building the image, one should upload it to a registry (i.e., docker push consert/hub:cu12.x)
Modify [./hub.yaml](./hub.yaml) with the image repo/tag, an INITIAL_PASSWORD, the desired storage (pvc), cpu, memory and gpu. If needed, modify the LoadBalancer.nodePort (if 32002 is not available).

Conda is not included, it can be installed later in the container if needed:

```shell
# https://docs.anaconda.com/free/miniconda/
INSTALLER_URL=https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
INSTALLER_SHA256=96a44849ff17e960eeb8877ecd9055246381c4d4f2d031263b63fa7e2e930af1

mkdir -p ~/miniconda3
wget $INSTALLER_URL -O ~/miniconda3/miniconda.sh
echo "$INSTALLER_SHA256  ~/miniconda3/miniconda.sh" | sha256sum -c
bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
rm -rf ~/miniconda3/miniconda.sh

# Add to PATH
~/miniconda3/bin/conda init bash
# or
# ~/miniconda3/bin/conda init zsh
```
