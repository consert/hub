#!/bin/env bash

# shellcheck disable=SC1091,SC1090

set -e
if [ "$(id -u)" = "0" ]; then exit 0; fi
pyenv init --path
pyenv init -
pyenv virtualenv-init -
mkdir -p "${HOME}/.local/venvs" && cd "${HOME}/.local/venvs" || exit 1

_SYSTEM_VERSION="$(python -c 'import sys; print(sys.version_info[1])')"

for v in 8 9 10 11 12; do
    if [ "${v}" != "${_SYSTEM_VERSION}" ]; then
        pyenv install "3.$v"
        mkdir "py3.$v" && cd "py3.$v" || exit 1
        pyenv global "3.$v"
        python --version
        python -m venv . && source bin/activate
        pip install --upgrade pip setuptools numpy wheel ipykernel
        py_version="$(python --version | awk '{print $2}')"
        python -m ipykernel install \
            --user \
            --env PATH "$(pwd)/bin:${PATH}" \
            --name "python${py_version}" \
            --display-name "Python ${py_version} (pyenv)"
        deactivate
        cd ..
    else
        echo "Python v${v} already exists in the system. Skipping.."
    fi
done
# use the system python version by default
pyenv global system
